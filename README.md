#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
#
# This is social handlers for od/vk/fb/live journal share links.
# And import friends from od/vk/fb
#
# Example of sharing links (see also share.js)
#           <div class="row">
#				<label>������������ ����������� �</label>
#				<ul class="social publish">
#					<li><a href="#" onclick="Share.facebook($('[name=public-link]').val(),$('[name=public-message]').val(),'','');" class="facebook">facebook</a></li>
#					<li><a href="#" onclick="Share.twitter($('[name=public-link]').val(),$('[name=public-message]').val());" class="twitter">twitter</a></li>
#					<li><a href="#" onclick="Share.vkontakte($('[name=public-link]').val(),$('[name=public-message]').val(),'','');" class="vkontakte">vkontakte</a></li>
#					<li><a href="#" onclick="Share.odnoklassniki($('[name=public-link]').val(),$('[name=public-message]').val());" class="odnoklasniki">odnoklasniki</a></li>
#					<li><a href="#" onclick="Share.livejournal($('[name=public-link]').val(),$('[name=public-message]').val(),'')" class="gg">livejournal</a></li>
#				</ul>
#			</div>
#
# Example importing friends (see also folder ../social_handlers/)
#
#    <ul class="social">
#		<li><a href="#" onclick="Share.import_friends_fb();" class="facebook">facebook</a></li>
#		<li><a href="#" onclick="Share.import_friends_vk();" class="vkontakte">vkontakte</a></li>
#		<li><a href="#" onclick="Share.import_friends_od();" class="odnoklasniki">odnoklasniki</a></li>
#	</ul>
#