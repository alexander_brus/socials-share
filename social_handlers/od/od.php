<?
#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
//Настройки приложения вконтакте
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
include '../func.php';

define('MOD_OD_ID', '128226816');
define('MOD_OD_PRIVATE', 'CBAHJFKIABABABABA');
define('MOD_OD_SECRET', '26CD618766EFED555558AF78');


include("odnoklassniki.php");

$redirect = 'http://'.$_SERVER['HTTP_HOST']."/social_handlers/od/od.php";
if(isset($_GET["code"]))
{
	$params = 'client_id='.MOD_OD_ID.'&client_secret='.MOD_OD_SECRET.'&grant_type=authorization_code&code='.htmlspecialchars($_GET['code']).'&redirect_uri='.$redirect;
	$txt = curl_post('http://api.odnoklassniki.ru/oauth/token.do', $params);
	$res = json_decode($txt);
	if(isset($res->error_msg))
	{
		echo $res->error_msg;
	}
	else
	{
		$access_token = $res->access_token;
		$refresh_token = $res->refresh_token;
		$par = get_signed_params(array('method' => 'users.getCurrentUser'), $access_token);
		$txt = curl_post('http://api.odnoklassniki.ru/fb.do?', $par);
		$user_data = json_decode($txt);

		$_SESSION["OD"] = array();
		$_SESSION["OD"]["USER_ID"] = $user_data->uid;
		$_SESSION["OD"]["ACCESS_TOKEN"] = $res->access_token;
		$_SESSION["OD"]["REFRESH_TOKEN"] = $res->refresh_token;
		main_redir("/social_handlers/od/od_get_friend_list.php");
	}
}else
{
	main_redir('http://www.odnoklassniki.ru/oauth/authorize?client_id='.MOD_OD_ID.'&scope='.urlencode('VALUABLE ACCESS').";".urlencode('PHOTO CONTENT').'&response_type=code&redirect_uri='.$redirect);
}
?>