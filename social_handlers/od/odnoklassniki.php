<?
#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
function get_sign($request_params, $token)
{
  ksort($request_params);
  $params = '';
  foreach ($request_params as $key => $value)
    $params.= "$key=$value";
  return md5($params.md5($token.MOD_OD_SECRET));
}

function get_signed_params($request_params, $token)
{
  $request_params['application_key'] = MOD_OD_PRIVATE;
  ksort($request_params);
  $params = $par = '';
  foreach ($request_params as $key => $value)
  {
    $params.= "$key=$value";
    $par.= ($par == '' ? '' : '&')."$key=$value";
  }
  $sig = md5($params.md5($token.MOD_OD_SECRET));
  $par.= '&access_token='.$token.'&sig='.$sig;
  return $par;
}?>