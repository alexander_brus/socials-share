<?
#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
include '../func.php';

define('MOD_OD_ID', '128226816');
define('MOD_OD_PRIVATE', 'CBAHJFKIABABABABA');
define('MOD_OD_SECRET', '26CD618766EFED555558AF78');


include("odnoklassniki.php");


function update_friends_list($uid, $token)
{
	$friend_list = array();
	
	$params = array('method' => 'friends.get');
	$par = get_signed_params($params, $token);
	$txt = curl_post('http://api.odnoklassniki.ru/fb.do?', $par);
	$ids = json_decode($txt);
	if(!is_array($ids))
	{
		return false;
	}
	else
	{
		if(count($ids) > 100)
		{
			$ids = array_chunk($ids, 100);
		}
		else
		{
			$ids = array(0 => $ids);
		}
		$friends = array();
		foreach($ids as $val)
		{
			$params = array('method' => 'users.getInfo', 'uids' => implode(',',$val), 'fields' => 'uid,name,birthday,location,pic_1,pic_2,pic_3,pic_4');
			$par = get_signed_params($params, $token);
			$txt = curl_post('http://api.odnoklassniki.ru/fb.do?', $par);
			$tmp = json_decode($txt);
			$friends = array_merge($friends, $tmp);
		}
		
		foreach($friends as $val)
		{
			$day = 0;
			$month = 0;
			$year = 0;
			$name = "";
			$name = $val->name;
			if($val->birthday != '')
			{
				$dt = $val->birthday;
				list($year,$month,$day) = explode("-",$dt);
				$year = intval($year);
				$month = intval($month);
				$day = intval($day);
			}
			else
			{
				$dt = '0000-00-00';
			}
			if($day>0 && $month>0)
			{
				$ident = $val->uid;
				$link = $val->url_profile;
			  
				//print_r($val);
				$friend_list[$val->uid] = array(
					"UID"=>$ident,
					"NAME"=>$name,
					"DATE_OF_BIRTH"=>$dt,
					"PICTURE"=>$val->pic_1,
					"CITY"=>$val->location->city,
					"DAY"=>$day,
					"MONTH"=>$month,
					"YEAR"=>$year
				);
			}
		}
	}
	return $friend_list;
}

function getMonthName($i)
{
	$__months = array("январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь");
	return $__months[$i-1];
}

function deleteFriend($uid)
{
	global $APPLICATION,$USER;
	if(CModule::IncludeModule("iblock"))
	{
		$dbEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_OD_ID"=>$uid));
		
		if($rsEl = $dbEl->GetNextElement())
		{
			$arEl = $rsEl->GetFields();
			$arEl["PROPERTIES"] = $rsEl->GetProperties();
			if(count($arEl["PROPERTIES"]["USER_ID"]["VALUE"])>1)
			{
				foreach($arEl["PROPERTIES"]["USER_ID"]["VALUE"] as $key=>$item)
				{
					if($item == $USER->GetID())
						unset($arEl["PROPERTIES"]["USER_ID"]["VALUE"][$key]);
				}
				CIBlockElement::SetPropertyValues($arEl["ID"],$arEl["IBLOCK_ID"],$arEl["PROPERTIES"]["USER_ID"]["VALUE"],"USER_ID");
			}
			else
			{
				CIBlockElement::Delete($arEl["ID"]);
			}
		}
	}
}

function createFriend($values)
{
	global $USER,$APPLICATION;
	$ID = 0;
	
	if(CModule::IncludeModule("iblock"))
	{
		
		$el = new CIBlockElement;
		$PROP = array(
			"NAME" => $values["NAME"],
			"AVATAR" => $values["PICTURE"],
			"DATE" => $values["DATE_OF_BIRTH"],
			"OD_ID" => $values["UID"],
			"CITY" => $values["CITY"],
			"DAY"=> $values["DAY"],
			"MONTH"=> $values["MONTH"],
			"YEAR"=>$values["YEAR"]
		);

		$dbEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_OD_ID"=>$values["UID"]));
		if($rsEl = $dbEl->GetNextElement())
		{
			$arEl = $rsEl->GetFields();
			$ID = $arEl["ID"];
			$arEl["PROPERTIES"] = $rsEl->GetProperties();
			$PROP["USER_ID"] = array_merge($arEl["PROPERTIES"]["USER_ID"]["VALUE"],array($USER->GetID()));
		}
		else
		{
			$PROP["USER_ID"] = array($USER->GetID());
		}
		
		$arLoadProductArray = Array(
			"IBLOCK_ID"      => 2,
			"IBLOCK_SECTION" => false,          // элемент лежит в корне раздела
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => $values["NAME"]." : ".$values["UID"],
			"ACTIVE"         => "Y"
		);

		if($ID>0)
		{
			$res = $el->Update($ID, $arLoadProductArray);
		}
		else
		{
			$ID = $el->Add($arLoadProductArray);
			echo $el->LAST_ERROR;
		}
	}
	return $ID;
}

$friends = update_friends_list($_SESSION["OD"]["USER_ID"], $_SESSION["OD"]["ACCESS_TOKEN"]);

if(isset($_REQUEST["friends"]))
{
	foreach($_REQUEST["friends"] as $u)
	{
		$user = array();
		if(intval($u)>0)
		{
			$user = $friends[$u];
			createFriend($user);
		}
	}
	foreach($_REQUEST["unfriends"] as $u)
	{
		if(intval($u)>0)
		{
			deleteFriend($u);
		}
	}
}


$arr__ODID = array();
CModule::IncludeModule("iblock");
global $USER;
$dbFriends = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_USER_ID"=>$USER->GetID(),"!PROPERTY_OD_ID"=>false));
while($rsFriend = $dbFriends->GetNextElement())
{
	$arFriend = $rsFriend->GetProperties();
	$arr__ODID[] = $arFriend["OD_ID"]["VALUE"];
}
?>
<link rel="stylesheet" type="text/css" href="../z_files/css/main.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="../z_files/js/main.js"></script>
<script>
	$(function(){
		var reload_0 = $('input[name=reload_parent]');
		if(reload_0.val() == "Y")	
		{
			window.opener.location.hash='tab-2';
			window.opener.location.reload();
			window.close();
		}
		$('.btn-cabinet.import').click(function(){
			$(this).parent().append("<input type='hidden'name='reload_parent' value='Y' />");
			$(this).parent().submit();
			
			//window.close();
			return false;
		});
	});
</script>
<form>
	<input type="submit" style="display:none" />
	<input type="hidden" name="reload_parent" value="<?=htmlspecialchars($_REQUEST["reload_parent"])?>" />
	<a href="#" class="btn-cabinet import"><span>Импортировать</span></a>
	<a href="#" class="btn-cabinet check" onclick="Friends.__checkAll();return false;"><span>Отметить всех друзей</span></a>
	<a href="#" onclick="Friends.__uncheckAll();return false;" class="btn-cabinet uncheck"><span>Убрать всех друзей</span></a>
	<br/>
	<br/>
	<ul class="friends-list"><?
	foreach($friends as $u)
	{
		?>
			<li id="<?=$u["UID"]?>" onclick="Friends.check(this)">
				<input type="hidden" name="friends[]" value="<?=((empty($arr__ODID)||in_array($u["UID"],$arr__ODID))?$u["UID"]:"")?>" />
				<input type="hidden" name="unfriends[]" value="<?=(!(empty($arr__ODID)||in_array($u["UID"],$arr__ODID))?$u["UID"]:"")?>" />
				<a <?=((in_array($u["UID"],$arr__ODID) || empty($arr__ODID))?"class='active'":"")?>>
					<span class="soc-icon odnoklasniki">&nbsp;</span>
					<img style="width:50px;height:50px;" alt="photo" src="<?=$u["PICTURE"]?>">
					<span class="friends-text">
						<strong class="name"><?=$u["NAME"]?></strong>
						<span class="date"><?
						list($year,$month,$day) = explode("-",$u["DATE_OF_BIRTH"]);
						echo $day." ".getMonthName($month)." ".$year;
						?></span>
						<em class="city"><?=$u["CITY"]?></em>
					</span>
					<?if(in_array($u["UID"],$arr__ODID) || empty($arr__ODID)):?>
						<span class="sing">&nbsp;</span>
					<?endif?>
				</a>
			</li>
		<?
	}
	?>
	</ul>
	<?if(count($friends)>12):?>
		<a href="#" class="btn-cabinet import"><span>Импортировать</span></a>
	<?endif?>
</form>