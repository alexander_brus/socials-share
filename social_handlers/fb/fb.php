<?
#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
//��������� ���������� ���������
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
include '../func.php';
require_once('facebook.php');
$config = array();
$config['appId'] = '358182780953837';
$config['secret'] = 'c152d9ddf89ecc36ef463333ff734a49';
$facebook = new Facebook($config);

$redirect = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];

if(isset($_GET["code"]))
{
	$txt = curl_get("https://graph.facebook.com/oauth/access_token?client_id=".$config['appId']."&redirect_uri=".urlencode($redirect)."&client_secret=".$config['secret']."&code=".$_GET['code']);
	if(!strstr($txt, 'access_token'))
	{
		die('������ �����������!');
	}
	else
	{
		$i = strpos($txt, '=');
		$token = substr($txt, $i + 1, strpos($txt, '&') - $i - 1);
		$facebook->setAccessToken($token);
		$user_id = $facebook->getUser();
		$_SESSION["FB"] = array();
		$_SESSION["FB"]["USER_ID"] = $user_id;
		$_SESSION["FB"]["ACCESS_TOKEN"] = $token;
		main_redir("/social_handlers/fb/fb_get_friend_list.php");
	}
}else
{
	main_redir($facebook->getLoginUrl(array('scope' => 'user_birthday,read_stream,friends_birthday,user_location,friends_location', 'redirect_uri' => $redirect)));
}
?>