<?
#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
include '../func.php';
require_once('facebook.php');
$config = array();
$config['appId'] = '358182780953837';
$config['secret'] = 'c152d9ddf89ecc36ef463333ff734a49';
$facebook = new Facebook($config);

function update_friends_list($uid, $token)
{
	$friend_list = array();
	
	global $facebook;
	$facebook->setAccessToken($token);
	$friends = $facebook->api('/me/friends','GET');

	if(!isset($friends['data']))
	{
		return false;
	}
	else
	{
		foreach($friends['data'] as $val)
		{
			$day = 0;
			$month = 0;
			$year = 0;
			$name = "";
			
			$user_profile = $facebook->api('/'.$val['id'].'?fields=id,name,birthday,link,location,picture','GET');
			$name = $user_profile['name'];
			if($user_profile['birthday'])
			{
			  $dt = explode('/', $user_profile['birthday']);
			  $day = intval($dt[1]);
			  $month = intval($dt[0]);
			  $year = intval($dt[2]);
			  $dt = $dt[2].'-'.$dt[0].'-'.$dt[1];
			}
			else
			{
				$dt = '0000-00-00';
			}
			if($day>0 && $month>0)
			{
				$ident = $user_profile['id'];
				$link = $user_profile['link'];
				
				$friend_list[$ident] = array(
					"UID"=>$ident,
					"NAME"=>$name,
					"DATE_OF_BIRTH"=>$dt,
					"PICTURE"=>$user_profile['picture']['data']['url'],
					"CITY"=>$user_profile['location']["name"],
					"DAY"=>$day,
					"MONTH"=>$month,
					"YEAR"=>$year
				);
			}
		}
	}
	return $friend_list;
}


function getMonthName($i)
{
	$__months = array("январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь");
	return $__months[$i-1];
}

function deleteFriend($uid)
{
	global $APPLICATION,$USER;
	if(CModule::IncludeModule("iblock"))
	{
		$dbEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_FB_ID"=>$uid));
		
		if($rsEl = $dbEl->GetNextElement())
		{
			$arEl = $rsEl->GetFields();
			$arEl["PROPERTIES"] = $rsEl->GetProperties();
			if(count($arEl["PROPERTIES"]["USER_ID"]["VALUE"])>1)
			{
				foreach($arEl["PROPERTIES"]["USER_ID"]["VALUE"] as $key=>$item)
				{
					if($item == $USER->GetID())
						unset($arEl["PROPERTIES"]["USER_ID"]["VALUE"][$key]);
				}
				CIBlockElement::SetPropertyValues($arEl["ID"],$arEl["IBLOCK_ID"],$arEl["PROPERTIES"]["USER_ID"]["VALUE"],"USER_ID");
			}
			else
			{
				CIBlockElement::Delete($arEl["ID"]);
			}
		}
	}
}

function createFriend($values/*,$city_list*/)
{
	global $USER,$APPLICATION;
	$ID = 0;
	
	if(CModule::IncludeModule("iblock"))
	{
		
		$el = new CIBlockElement;
		$PROP = array(
			"NAME" => $values["NAME"],
			"AVATAR" => $values["PICTURE"],
			"DATE" => $values["DATE_OF_BIRTH"],
			"FB_ID" => $values["UID"],
			"CITY" => $values["CITY"],
			"DAY"=> $values["DAY"],
			"MONTH" => $values["MONTH"],
			"YEAR" => $values["YEAR"]
		);

		$dbEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_FB_ID"=>$values["UID"]));
		if($rsEl = $dbEl->GetNextElement())
		{
			$arEl = $rsEl->GetFields();
			$ID = $arEl["ID"];
			$arEl["PROPERTIES"] = $rsEl->GetProperties();
			$PROP["USER_ID"] = array_merge($arEl["PROPERTIES"]["USER_ID"]["VALUE"],array($USER->GetID()));
		}
		else
		{
			$PROP["USER_ID"] = array($USER->GetID());
		}
		
		$arLoadProductArray = Array(
			"IBLOCK_ID"      => 2,
			"IBLOCK_SECTION" => false,          // элемент лежит в корне раздела
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => $values["NAME"]." : ".$values["UID"],
			"ACTIVE"         => "Y"
		);

		if($ID>0)
		{
			$res = $el->Update($ID, $arLoadProductArray);
		}
		else
		{
			$ID = $el->Add($arLoadProductArray);
			echo $el->LAST_ERROR;
		}
	}
	return $ID;
}

$friends = update_friends_list($_SESSION["FB"]["USER_ID"], $_SESSION["FB"]["ACCESS_TOKEN"]);


if(isset($_REQUEST["friends"]))
{
	foreach($_REQUEST["friends"] as $u)
	{
		$user = array();
		if(intval($u)>0)
		{
			$user = $friends[$u];
			createFriend($user);
		}
	}
	foreach($_REQUEST["unfriends"] as $u)
	{
		if(intval($u)>0)
		{
			deleteFriend($u);
		}
	}
}


$arr__FBID = array();
CModule::IncludeModule("iblock");
global $USER;
$dbFriends = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_USER_ID"=>$USER->GetID(),"!PROPERTY_FB_ID"=>false));
while($rsFriend = $dbFriends->GetNextElement())
{
	$arFriend = $rsFriend->GetProperties();
	if(intval($arFriend["FB_ID"]["VALUE"])>0)
		$arr__FBID[] = $arFriend["FB_ID"]["VALUE"];
}

?>
<link rel="stylesheet" type="text/css" href="../z_files/css/main.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="../z_files/js/main.js"></script>
<script>
	$(function(){
		var reload_0 = $('input[name=reload_parent]');
		if(reload_0.val() == "Y")	
		{
			window.opener.location.hash='tab-2';
			window.opener.location.reload();
			window.close();
		}
		$('.btn-cabinet.import').click(function(){
			$(this).parent().append("<input type='hidden'name='reload_parent' value='Y' />");
			$(this).parent().submit();
			
			//window.close();
			return false;
		});
	});
</script>
<form>
	<input type="submit" style="display:none" />
	<input type="hidden" name="reload_parent" value="<?=htmlspecialchars($_REQUEST["reload_parent"])?>" />
	<a href="#" class="btn-cabinet import"><span>Импортировать</span></a>
	<a href="#" class="btn-cabinet check" onclick="Friends.__checkAll();return false;"><span>Отметить всех друзей</span></a>
	<a href="#" onclick="Friends.__uncheckAll();return false;" class="btn-cabinet uncheck"><span>Убрать всех друзей</span></a>
	<br/>
	<br/>
	<ul class="friends-list"><?
	foreach($friends as $u)
	{
		?>
			<li id="<?=$u["UID"]?>" onclick="Friends.check(this)">
				<input type="hidden" name="friends[]" value="<?=((empty($arr__FBID)||in_array($u["UID"],$arr__FBID))?$u["UID"]:"")?>" />
				<input type="hidden" name="unfriends[]" value="<?=(!(empty($arr__FBID)||in_array($u["UID"],$arr__FBID))?$u["UID"]:"")?>" />
				<a <?=((in_array($u["UID"],$arr__FBID) || empty($arr__FBID))?"class='active'":"")?>>
					<span class="soc-icon facebook">&nbsp;</span>
					<img alt="photo" src="<?=$u["PICTURE"]?>">
					<span class="friends-text">
						<strong class="name"><?=$u["NAME"]?></strong>
						<span class="date"><?
						list($year,$month,$day) = explode("-",$u["DATE_OF_BIRTH"]);
						echo $day." ".getMonthName($month)." ".$year;
						?></span>
						<em class="city"><?=$u["CITY"]?></em>
					</span>
					<?if(in_array($u["UID"],$arr__FBID) || empty($arr__FBID)):?>
						<span class="sing">&nbsp;</span>
					<?endif?>
				</a>
			</li>
		<?
	}
	?>
	</ul>
	<?if(count($friends)>12):?>
		<a href="#" class="btn-cabinet import"><span>Импортировать</span></a>
	<?endif?>
</form>