<?
#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
//��������� ���������� ���������
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
include '../func.php';
define('MOD_VK_APPID', '3404924');//ID ����������
define('MOD_VK_APPKEY', 'f3G341pHLUmra1Knxbxd');//���� ����������

$redirect = 'http://'.$_SERVER['HTTP_HOST']."/social_handlers/vk/vk.php";

if(isset($_GET["code"]))
{
	$txt = curl_get('https://oauth.vk.com/access_token?client_id='.MOD_VK_APPID.'&client_secret='.MOD_VK_APPKEY.'&code='.htmlspecialchars($_GET['code']).'&redirect_uri='.$redirect);
	$res = json_decode($txt);
	if(isset($res->error))
	{
		echo $res->error_description;
	}
	else
	{
		$_SESSION["VK"] = array();
		$_SESSION["VK"]["USER_ID"] = $res->user_id;
		$_SESSION["VK"]["ACCESS_TOKEN"] = $res->access_token;
		main_redir("/social_handlers/vk/vk_get_friend_list.php");//?token=".$res->access_token."&user_id=".$res->user_id);
	}
}else
{
	main_redir("https://oauth.vk.com/authorize?client_id=".MOD_VK_APPID."&scope=notify,friends,offline&redirect_uri=".$redirect."&display=page&response_type=code");
}
?>