<?
#################################################
#   Developer: Alexander Brus                   #
#   Site:                                       #
#   E-mail: alexghostalex@gmail.com             #
#   Copyright (c) 2012-2013 Alexander Brus      #
#################################################
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
include '../func.php';
function update_friends_list($uid, $token)
{
	$friend_list = array();
	$txt = curl_get('https://api.vk.com/method/friends.get?uid='.$uid.'&fields=uid,first_name,last_name,bdate,photo,city&access_token='.$token);
	if(strstr($txt, 'error_description'))
	{
		return false;
	}
	else
	{
		$tmp = json_decode($txt);
		foreach($tmp->response as $val)
		{
			$day = 0;
			$month = 0;
			$year = 0;
			$name = "";
			
			$name = $val->last_name.' '.$val->first_name;//iconv('utf-8','cp1251',$val->last_name.' '.$val->first_name);
			if($val->bdate != '')
			{
				$dt = explode('.', $val->bdate);
				$day = intval($dt[0]);
				$month = intval($dt[1]);
				$year = intval($dt[2]);
				$dt = $dt[2].'-'.$dt[1].'-'.$dt[0];
			}
			else
			{
				$dt = '0000-00-00';
			}
			if(intval($day)>0 && intval($month)>0)
			{
				$ident = $val->uid;
				$friend_list[$val->uid] = array(
					"UID"=>$ident,
					"NAME"=>$name,
					"DATE_OF_BIRTH"=>$dt,
					"PICTURE"=>$val->photo,
					"CITY"=>$val->city,
					"DAY"=>$day,
					"MONTH"=>$month,
					"YEAR"=>$year
				);
			}
		}
	}
	return $friend_list;
}

function getCities($uid,$token,$cities)
{
	$city = array();
	$txt = curl_get('https://api.vk.com/method/places.getCityById?cids='.implode(',',$cities).'&access_token='.$token);
	if(strstr($txt, 'error_description'))
	{
		return false;
	}
	else
	{
		$tmp = json_decode($txt);
		foreach($tmp->response as $val)
		{
			$city[$val->cid] = $val->name;
		}
	}
	return $city;
}

function getMonthName($i)
{
	$__months = array("январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь");
	return $__months[$i-1];
}
function deleteFriend($uid)
{
	global $APPLICATION,$USER;
	if(CModule::IncludeModule("iblock"))
	{
		$dbEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_VK_ID"=>$uid));
		
		if($rsEl = $dbEl->GetNextElement())
		{
			$arEl = $rsEl->GetFields();
			$arEl["PROPERTIES"] = $rsEl->GetProperties();
			if(count($arEl["PROPERTIES"]["USER_ID"]["VALUE"])>1)
			{
				foreach($arEl["PROPERTIES"]["USER_ID"]["VALUE"] as $key=>$item)
				{
					if($item == $USER->GetID())
						unset($arEl["PROPERTIES"]["USER_ID"]["VALUE"][$key]);
				}
				CIBlockElement::SetPropertyValues($arEl["ID"],$arEl["IBLOCK_ID"],$arEl["PROPERTIES"]["USER_ID"]["VALUE"],"USER_ID");
			}
			else
			{
				CIBlockElement::Delete($arEl["ID"]);
			}
		}
	}
}

function createFriend($values,$city_list)
{
	global $USER,$APPLICATION;
	$ID = 0;
	
	if(CModule::IncludeModule("iblock"))
	{
		$el = new CIBlockElement;
		$PROP = array(
			"NAME" => $values["NAME"],
			"AVATAR" => $values["PICTURE"],
			"DATE" => $values["DATE_OF_BIRTH"],
			"VK_ID" => $values["UID"],
			"CITY" => $city_list[$values["CITY"]],
			"DAY"=>$values["DAY"],
			"MONTH"=>$values["MONTH"],
			"YEAR"=>$values["YEAR"]
		);

		$dbEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_VK_ID"=>$values["UID"]));
		if($rsEl = $dbEl->GetNextElement())
		{
			$arEl = $rsEl->GetFields();
			$ID = $arEl["ID"];
			$arEl["PROPERTIES"] = $rsEl->GetProperties();
			$PROP["USER_ID"] = array_merge($arEl["PROPERTIES"]["USER_ID"]["VALUE"],array($USER->GetID()));
		}
		else
		{
			$PROP["USER_ID"] = array($USER->GetID());
		}
		
		$arLoadProductArray = Array(
			"IBLOCK_ID"      => 2,
			"IBLOCK_SECTION" => false,          // элемент лежит в корне раздела
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => $values["NAME"]." : ".$values["UID"],
			"ACTIVE"         => "Y"
		);

		if($ID>0)
		{
			$res = $el->Update($ID, $arLoadProductArray);
		}
		else
		{
			$ID = $el->Add($arLoadProductArray);
			echo $el->LAST_ERROR;
		}
	}
	return $ID;
}

$friends = update_friends_list($_SESSION["VK"]["USER_ID"], $_SESSION["VK"]["ACCESS_TOKEN"]);
$cities = array();
foreach($friends as $u)
{
	if($u["CITY"]>0 && !in_array($u["CITY"],$cities))
		$cities[] = $u["CITY"];
}

$city_list = getCities($_SESSION["VK"]["USER_ID"], $_SESSION["VK"]["ACCESS_TOKEN"],$cities);


if(isset($_REQUEST["friends"]))
{
	foreach($_REQUEST["friends"] as $u)
	{
		$user = array();
		if(intval($u)>0)
		{
			$user = $friends[$u];
			createFriend($user,$city_list);
		}
	}
	foreach($_REQUEST["unfriends"] as $u)
	{
		if(intval($u)>0)
		{
			deleteFriend($u);
		}
	}
}


$arr__VKID = array();
CModule::IncludeModule("iblock");
global $USER;
$dbFriends = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"PROPERTY_USER_ID"=>$USER->GetID(),"!PROPERTY_VK_ID"=>false));
while($rsFriend = $dbFriends->GetNextElement())
{
	$arFriend = $rsFriend->GetProperties();
	$arr__VKID[] = $arFriend["VK_ID"]["VALUE"];
}

?>
<link rel="stylesheet" type="text/css" href="../z_files/css/main.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="../z_files/js/main.js"></script>
<script>
	$(function(){
		var reload_0 = $('input[name=reload_parent]');
		if(reload_0.val() == "Y")	
		{
			window.opener.location.hash='tab-2';
			window.opener.location.reload();
			window.close();
		}
		$('.btn-cabinet.import').click(function(){
			$(this).parent().append("<input type='hidden'name='reload_parent' value='Y' />");
			$(this).parent().submit();
			
			//window.close();
			return false;
		});
	});
</script>
<form>
	<input type="submit" style="display:none" />
	<input type="hidden" name="reload_parent" value="<?=htmlspecialchars($_REQUEST["reload_parent"])?>" />
	<a href="#" class="btn-cabinet import"><span>Импортировать</span></a>
	<a href="#" class="btn-cabinet check" onclick="Friends.__checkAll();return false;"><span>Отметить всех друзей</span></a>
	<a href="#" onclick="Friends.__uncheckAll();return false;" class="btn-cabinet uncheck"><span>Убрать всех друзей</span></a>
	<br/>
	<br/>
	<ul class="friends-list"><?
	foreach($friends as $u)
	{
		?>
			<li id="<?=$u["UID"]?>" onclick="Friends.check(this)">
				<input type="hidden" name="friends[]" value="<?=((empty($arr__VKID)||in_array($u["UID"],$arr__VKID))?$u["UID"]:"")?>" />
				<input type="hidden" name="unfriends[]" value="<?=(!(empty($arr__VKID)||in_array($u["UID"],$arr__VKID))?$u["UID"]:"")?>" />
				<a <?=((in_array($u["UID"],$arr__VKID) || empty($arr__VKID))?"class='active'":"")?>>
					<span class="soc-icon vkontakte">&nbsp;</span>
					<img alt="photo" src="<?=$u["PICTURE"]?>">
					<span class="friends-text">
						<strong class="name"><?=$u["NAME"]?></strong>
						<span class="date"><?
						list($year,$month,$day) = explode("-",$u["DATE_OF_BIRTH"]);
						echo $day." ".getMonthName($month)." ".$year;
						?></span>
						<em class="city"><?=$city_list[$u["CITY"]]?></em>
					</span>
					<?if(in_array($u["UID"],$arr__VKID) || empty($arr__VKID)):?>
						<span class="sing">&nbsp;</span>
					<?endif?>
				</a>
			</li>
		<?
	}
	?>
	</ul>
	<?if(count($friends)>12):?>
		<a href="#" class="btn-cabinet import"><span>Импортировать</span></a>
	<?endif?>
</form>