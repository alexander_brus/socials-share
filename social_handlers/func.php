<?
function main_redir($addr, $close_conn = true, $code = 'HTTP/1.1 303 See Other')
{
  header($code);
  header('location: '.$addr);
  if ($close_conn)
  {
    mysql_close();
    exit();
  }
}

function err_exit($err_txt = '', $url = '')
{
  $_SESSION['err_msg'] = !empty($err_txt) ? $err_txt : 'Внутренняя ошибка! Обратитесь к администрации сервиса.';
  if(empty($url)) $url = $_SERVER['PHP_SELF'];
  main_redir($url);
}

function ok_exit($ok_txt = '', $url = '')
{
  $_SESSION['ok_msg'] = !empty($ok_txt) ? $ok_txt : 'Операция успешно завершена!';
  if(empty($url)) $url = $_SERVER['PHP_SELF'];
  main_redir($url);
}

/*function check_email($email)
{  return ($email != '' && preg_match('/^[a-z0-9\_\-\.]*\@[a-z0-9\_\-\.]*\.[a-z]/',$email));}*/

function format_date($timestamp)
{  $Month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
  $Day = array('воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота');
  if(date("w") == 1)
  {
    $week_begin = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
  }
  else
  {
  	$week_begin = strtotime("last Monday");
  }
	if($timestamp > mktime(0, 0, 0, date('m'), date('d'), date('Y')))
	{		//Сегодня
    return date('H:i', $timestamp);
	}
	elseif($timestamp > (mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 86400))
	{    //Вчера
    return 'вчера, '.date('H:i', $timestamp);
	}
	elseif($timestamp > $week_begin)
	{    //На этой неделе
    return $Day[date('w', $timestamp)].', '.date('H:i', $timestamp);	}  elseif($timestamp > mktime(0, 0, 0, 1, 1, date('Y')))
  {  	//В этом году
    return date('d', $timestamp).' '.$Month[date('m', $timestamp) - 1];
  }
  else
  {  	//Очень давно
    return date('d', $timestamp).' '.$Month[date('m', $timestamp) - 1].' '.date('Y', $timestamp);
  }
}

function authorise($user_data)
{  $_SESSION['logged'] = true;
  $_SESSION['userid'] = intval($user_data['id']);
  $_SESSION['user_name'] = $user_data['user_name'];
}

function log_out()
{	unset($_SESSION['logged']);
	unset($_SESSION['userid']);
	unset($_SESSION['user_name']);
	session_destroy();
}

function curl_get($url)
{
  $cd = curl_init();
  curl_setopt($cd, CURLOPT_URL, $url);
  curl_setopt($cd, CURLOPT_HEADER, 0);
  curl_setopt($cd, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($cd, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($cd, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($cd, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($cd, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.186 Safari/535.1');
  curl_setopt($cd, CURLOPT_TIMEOUT, 1000);
  $page = curl_exec($cd);
  curl_close($cd);
  return $page;
}

function curl_post($url, $params)
{
  $cd = curl_init();
  curl_setopt($cd, CURLOPT_URL, $url);
  curl_setopt($cd, CURLOPT_HEADER, 0);
  curl_setopt($cd, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($cd, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($cd, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($cd, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($cd, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.186 Safari/535.1');
  curl_setopt($cd, CURLOPT_TIMEOUT, 1000);
  curl_setopt($cd, CURLOPT_POST, 1);
  curl_setopt($cd, CURLOPT_POSTFIELDS, $params);

  $page = curl_exec($cd);
  curl_close($cd);
  return $page;
}

?>