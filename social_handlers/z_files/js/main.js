var Friends = {
	check: function(el)
	{
		$el = $(el);
		$a = $el.find('a');
		$input = $el.find('input[name=friends\\[\\]]');
		$input2 = $el.find('input[name=unfriends\\[\\]]');
		if($a.hasClass("active"))
		{
			$a.removeClass("active");
			$a.find('.sing').remove();
			$input.val("0");
			$input2.val($el.attr("id"));
		}
		else
		{
			$a.addClass("active");
			$a.append('<span class="sing">&nbsp;</span>');
			$input.val($el.attr("id"));
			$input2.val("0");
		}
	},
	__checkAll:function(){
		$('ul.friends-list li').each(function(k,v){
			if(!$(this).find('a').hasClass('active'))
				Friends.check(this);
		});
	},
	__uncheckAll: function(){
		$('ul.friends-list li').each(function(k,v){
			if($(this).find('a').hasClass('active'))
				Friends.check(this);
		});
	}
};